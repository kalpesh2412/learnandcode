#Program to roll a dice

import random

def generateRandomNumber(maxRange):
    randomNumber=random.randint(1, maxRange)
    return randomNumber


def main():
    maxdiceRange=6
    diceReady=True
    while diceReady:
        enterChoice=input("Ready to roll? Enter Q to Quit")
        if enterChoice.lower() !="q":
            luckyNumber=generateRandomNumber(maxdiceRange)
            print("You have rolled a",luckyNumber)
        else:
            diceReady=False
 
